Version 4
SymbolType BLOCK
LINE Normal 0 128 0 112
LINE Normal 0 -96 -16 -112
LINE Normal 16 -112 0 -96
LINE Normal 96 0 80 0
LINE Normal 0 -112 0 -128
LINE Normal -80 0 -96 0
RECTANGLE Normal 80 112 -80 -112
TEXT 0 112 Bottom 1 Reset
TEXT -75 0 Left 1 D
TEXT 74 0 Right 1 Q
WINDOW 0 0 -48 Bottom 1
PIN 0 -128 NONE 8
PINATTR PinName Clk
PINATTR SpiceOrder 1
PIN 96 0 NONE 8
PINATTR PinName Q
PINATTR SpiceOrder 2
PIN -96 0 NONE 8
PINATTR PinName D
PINATTR SpiceOrder 3
PIN 0 128 NONE 8
PINATTR PinName Reset
PINATTR SpiceOrder 4
