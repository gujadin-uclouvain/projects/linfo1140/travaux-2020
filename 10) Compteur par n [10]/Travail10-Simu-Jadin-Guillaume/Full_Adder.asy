Version 4
SymbolType BLOCK
LINE Normal 48 -96 48 -80
LINE Normal -112 -16 -112 -80
LINE Normal -32 0 -112 -16
LINE Normal -112 16 -32 0
LINE Normal -112 80 -112 16
LINE Normal -112 -112 -112 -80
LINE Normal 48 -112 -112 -128
LINE Normal -112 112 -112 80
LINE Normal 48 -112 48 -96
LINE Normal 48 96 48 -80
LINE Normal -112 128 48 112
LINE Normal 48 96 48 112
LINE Normal -112 128 -112 112
LINE Normal -112 -128 -112 -112
LINE Normal 80 0 48 0
LINE Normal -144 -64 -112 -64
LINE Normal -144 64 -112 64
LINE Normal -32 144 -32 120
LINE Normal -32 -144 -32 -120
TEXT -1 3 Center 4 +
TEXT 42 0 Right 1 S
TEXT -32 -120 Top 1 C_in
TEXT -107 -64 Left 1 A
TEXT -106 64 Left 1 B
TEXT -32 115 Bottom 1 C_out
WINDOW 0 73 -123 Bottom 2
PIN -144 64 NONE 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -144 -64 NONE 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN -32 -144 NONE 8
PINATTR PinName C_in
PINATTR SpiceOrder 3
PIN -32 144 NONE 8
PINATTR PinName C_out
PINATTR SpiceOrder 4
PIN 80 0 NONE 8
PINATTR PinName S
PINATTR SpiceOrder 5
